#include <string>
#include <iostream>
#include "rcipher.h"

// -------------------------------------------------------
// ROT13 Cipher implementation
// -------------------------------------------------------
struct RCipher::CipherCheshire {
    int key;
    string cipher_alpha;
};

// -------------------------------------------------------
// -------------------------------------------------------

/* This is the default constructor
 */
RCipher::RCipher()
{
    // TODO: Implement this constructor
    smile = new CipherCheshire;
    smile->key = 13;
    std::string alphabet("abcdefghijklmnopqrstuvwxyz");
    //rotate alphabet by key
    rotate_string(alphabet, smile->key);
    smile->cipher_alpha = alphabet;
}

/* Destructor
 */
RCipher::~RCipher()
{
    // TODO: Implement this constructor
    //unsure about this
    
}


