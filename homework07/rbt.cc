#include "rbt.h"

// ---------------------------------------
// Node class
// Default constructor
RBTNode::RBTNode() : Node() {
    color = BLACK;
}

// Constructor
RBTNode::RBTNode(int in) : Node(in) {
    color = BLACK;
}
// Destructor
RBTNode::~RBTNode() {
}

void RBTNode::add_color(Node_color c)
{
  color = c;
}

void RBTNode::print_color(ostream& to)
{
    if(color == RED) {
        to << "Red";
    } else if (color == BLACK) {
        to << "Black";
    } else {
        cerr << "ERR: invalid color" << endl;
    }
}
void RBTNode::print_info(ostream& to)
{
    to << get_key() << " ";
    print_color(to);
}

Node_color RBTNode::get_color()
{
  return color;
}
// ---------------------------------------


// ---------------------------------------
// RBT class
// Constructor and destructor
RBT::RBT() : BST()
{
  sentinel = new RBTNode(-1);
  root = sentinel;
}
RBT::~RBT()
{
    // re-using BST's inorder_free
    inorder_free(root, sentinel);
    // This makes sure that root is set to nullptr, so that the parent class's
    // constructor does not try to free the tree again
    root = nullptr;
    delete sentinel; 
}

// Functions that are basically wrappers for the parent class functions
// minimum key in the BST
RBTNode* RBT::tree_min()
{
    // return (RBTNode*) get_min(root);
    // return (RBTNode*) BST::tree_min();
    return (RBTNode*) BST::get_min(root, sentinel);
}
// maximum key in the BST
RBTNode* RBT::tree_max()
{
    // return (RBTNode*) get_max(root);
    // return (RBTNode*) BST::tree_max();
    return (RBTNode*) BST::get_max(root, sentinel);
}

// Get successor of the given node
RBTNode* RBT::get_succ(RBTNode* in)
{
  return (RBTNode*) BST::get_succ((Node*) in, sentinel);
}

// Get predecessor of the given node
RBTNode* RBT::get_pred(RBTNode* in)
{
  return (RBTNode*) BST::get_pred((Node*) in, sentinel);
}

// Search the tree for a given key
RBTNode* RBT::tree_search(int search_key)
{
  return (RBTNode*) BST::tree_search(search_key, sentinel);
}

void RBT::walk(ostream& to)
{
  BST::inorder_walk(root, to, sentinel);
}



// New functions to RBT
// right rotate
void RBT::right_rotate(RBTNode* y)
{
    /* TODO */
    //temp pointers to hold x's parent and parent's parent
    if(y == sentinel){
        return;
    }
    Node* x = y->get_left();
    if(x == sentinel){
        return;
    }
    y->add_left(x->get_right());

    if(x->get_right() != sentinel){
        x->get_right()->add_parent(y);
    }
    x->add_parent(y->get_parent());

    if(y->get_parent() == sentinel){
        root = x;
    } else if (y->get_parent()->get_left() == y){
        y->get_parent()->add_left(x);
    } else {
        y->get_parent()->add_right(x);
    }
    
    x->add_right(y);
    y->add_parent(x);
}

// Left rotate
void RBT::left_rotate(RBTNode* x)
{
    /* TODO */
    //temp pointers to hold x's parent and parent's parent
    if(x == sentinel){
        return;
    }
    Node* x_parent = x->get_parent();
    Node* y = x->get_right();
    if(y == sentinel){
        return;
    }
    x->add_right(y->get_left());

    if(y->get_left() != sentinel){
        y->get_left()->add_parent(x);
    }
    y->add_parent(x->get_parent());

    if(x_parent == sentinel){
        root = y;
    } else if (x_parent->get_left() == x){
        x_parent->add_left(y);
    } else {
        x_parent->add_right(y);
    }
    
    y->add_left(x);
    x->add_parent(y);
}

void RBT::rb_insert_fixup(RBTNode* in)
{
    /* TODO */
    //I assume this is where the recoloring and rotation happens
    while((in != root) && (((RBTNode*)in->get_parent())->get_color() == RED)){
        RBTNode* p = (RBTNode*)in->get_parent();
        RBTNode* gp = (RBTNode*)p->get_parent();

        if(p == gp->get_left()){
            //Case 1
            RBTNode* y = ((RBTNode*)gp->get_right());
            if(y->get_color() == RED){
                p->add_color(BLACK);
                y->add_color(BLACK);
                gp->add_color(RED);
                in = gp;
                p = (RBTNode*)in->get_parent();
                gp = (RBTNode*)p->get_parent();

            } else {
                if(in == p->get_right()) {
                    //Case 2
                    in = (RBTNode*)p;
                    left_rotate(in);
                    p = (RBTNode*)in->get_parent();
                    gp = (RBTNode*)p->get_parent();
                }
                //Case 3
                p->add_color(BLACK);
                if(gp != sentinel){
                    gp->add_color(RED);
                }
                right_rotate((RBTNode*)gp);
            }

        } else {
            //Case 1
            RBTNode* y = ((RBTNode*)gp->get_left());
            if(y->get_color() == RED){
                p->add_color(BLACK);
                y->add_color(BLACK);
                if(gp != sentinel){
                    gp->add_color(RED);
                }
                in = gp;
                p = (RBTNode*)in->get_parent();
                gp = (RBTNode*)p->get_parent();

            } else {
                if(in == p->get_left()) {
                    //Case 2
                    in = p;
                    right_rotate(in);
                    p = (RBTNode*)in->get_parent();
                    gp = (RBTNode*)p->get_parent();
                }
                //Case 3
                p->add_color(BLACK);
                if(gp != sentinel){
                    gp->add_color(RED);
                }
                left_rotate(gp);
            } 
        }
    }
    ((RBTNode*)root)->add_color(BLACK);
}

void RBT::rb_insert_node(RBTNode* in)
{
    /* TODO */
    //I assume this is where the physical insert happens
    sentinel->add_parent(sentinel);
    sentinel->add_left(sentinel);
    sentinel->add_right(sentinel);

    insert_node(in, sentinel);
    in->add_left(sentinel);
    in->add_right(sentinel);
    in->add_color(RED);
    //fix up violations
    rb_insert_fixup(in);
}

void RBT::rb_delete_fixup(RBTNode* in)
{
    /* TODO */
    sentinel->add_parent(sentinel);
    sentinel->add_left(sentinel);
    sentinel->add_right(sentinel);
    while((in != root) && in->get_color() == BLACK){
        RBTNode* w;
        if(in == in->get_parent()->get_left()){
            w = (RBTNode*)in->get_parent()->get_right();
            if(w->get_color() == RED){
                w->add_color(BLACK);
                ((RBTNode*)in->get_parent())->add_color(RED);
                left_rotate((RBTNode*)in->get_parent());
                w = (RBTNode*)in->get_parent()->get_right();
            }
            if((((RBTNode*)w->get_left())->get_color() == BLACK) &&
                ((RBTNode*)w->get_right())->get_color() == BLACK){
                    w->add_color(RED);
                    in = (RBTNode*)in->get_parent();
            } else {
                if (((RBTNode*)w->get_right())->get_color() == BLACK){
                    ((RBTNode*)w->get_left())->add_color(BLACK);
                    w->add_color(RED);
                    right_rotate(w);
                    w = (RBTNode*)in->get_parent()->get_right();
                }
                w->add_color(((RBTNode*)in->get_parent())->get_color());
                ((RBTNode*)in->get_parent())->add_color(BLACK);
                ((RBTNode*)w->get_right())->add_color(BLACK);
                left_rotate((RBTNode*)in->get_parent());
                in = (RBTNode*)root;
            }
        } else {
            w = (RBTNode*)in->get_parent()->get_left();
            if(w->get_color() == RED){
                w->add_color(BLACK);
                ((RBTNode*)in->get_parent())->add_color(RED);
                right_rotate((RBTNode*)in->get_parent());
                w = (RBTNode*)in->get_parent()->get_left();
            }
            if((((RBTNode*)w->get_right())->get_color() == BLACK) &&
                ((RBTNode*)w->get_left())->get_color() == BLACK){
                    w->add_color(RED);
                    in = (RBTNode*)in->get_parent();
            } else {
                if (((RBTNode*)w->get_left())->get_color() == BLACK){
                    ((RBTNode*)w->get_right())->add_color(BLACK);
                    w->add_color(RED);
                    left_rotate(w);
                    w = (RBTNode*)in->get_parent()->get_left();
                }
                w->add_color(((RBTNode*)in->get_parent())->get_color());
                ((RBTNode*)in->get_parent())->add_color(BLACK);
                ((RBTNode*)w->get_left())->add_color(BLACK);
                right_rotate((RBTNode*)in->get_parent());
                in = (RBTNode*)root;
            }
        }
    }
    in->add_color(BLACK);
}

void RBT::rb_delete_node(RBTNode* out)
{
    /* TODO */
    // temporary pointers
    Node* y = sentinel;
    Node* x = sentinel;
    sentinel->add_parent(sentinel);
    sentinel->add_left(sentinel);
    sentinel->add_right(sentinel);

    // first determine if out has at most one child
    if((out->get_left() == sentinel) || (out->get_right() == sentinel)) {
        // 0 or 1 child - out will be spliced
        y = out;
    } else {
        // 2 children - out's successor will be sliced out
        y = this->get_succ(out);
        // cout << y->get_key() << endl;
    }
 
    // set x to a non-null child of y, or null if it has no children
    // Recall that y is either out (if it has 0 or 1 child), 
    // or succ (if it has 2 children)
    // Since succ can only have 1 child, y should only have one child
    if(y->get_left() != sentinel) {
        x = y->get_left();                            
    } else {
        x = y->get_right();
    }

    x->add_parent(y->get_parent());

    /*
    // Extra code to account for RBT
    if(x == nilptr && x != nullptr) {
        x->add_parent(y->get_parent());
    }
     */

    if(y->get_parent() == sentinel) {
        root = x;
    } else if(y == y->get_parent()->get_left()) {
        y->get_parent()->add_left(x);
    } else {
        y->get_parent()->add_right(x);
    }

    // Finally, replace out with y
    if(y != out) {
        // first copy out's parent and children to y
        y->add_parent(out->get_parent());
        if(y->get_parent() == sentinel) {
            root = y;
        } else {
            // update the parent's child to y
            if(out == out->get_parent()->get_left()) {
                y->get_parent()->add_left(y);
            } else {
                y->get_parent()->add_right(y);
            }
        }
        y->add_left(out->get_left());
        y->add_right(out->get_right());

        // update the children's parents to y
        if(y->get_left() != sentinel) {
            y->get_left()->add_parent(y);
        }
        if(y->get_right() != sentinel) {
            y->get_right()->add_parent(y);
        }
    }
    if(((RBTNode*)y)->get_color() == BLACK){
        rb_delete_fixup((RBTNode*)x);
    }
    // out is no longer needed - free it
    delete out;
}

// ---------------------------------------
