#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"
#include "vcipher.h"


struct VCipher::CipherCheshire {
    vector<string> cipher_alpha;
    int page_num;
};


// -------------------------------------------------------
// Vigenere Cipher implementation
// -------------------------------------------------------

VCipher::VCipher()
{
    // TODO: Implement this default constructor
    smile = new CipherCheshire;
    //TODO: figure out what to make the default key
    smile->page_num = 0;
    smile->cipher_alpha.emplace_back("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
}

/* This constructor initiates the object with a
   input cipher key
 */
VCipher::VCipher(string cipher_alpha)
{
    // TODO: Implement this constructor
    smile = new CipherCheshire;
    unsigned int length = cipher_alpha.size();
    string temp;
    for(unsigned int i = 0; i < MAX_LENGTH; i++){
        if(i < length){
            temp += cipher_alpha[i];
        } else {
            //i is more than characters in keyword so use modulo
            unsigned int key_pos = i % length;
            temp += cipher_alpha[key_pos];
        }
    }
    smile->page_num = 0;
    smile->cipher_alpha.emplace_back(temp);
}

/* Destructor
 */
VCipher::~VCipher()
{
    // TODO: Implement this constructor
    //unsure about this
    
}

