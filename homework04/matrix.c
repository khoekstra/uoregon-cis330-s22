#include "matrix.h"

/* Debugging code that prints the BFS result for a particular iteration
   input parameters:
       int  rows	# of vertices
       int* color	array of color for the vertices
       int* distance	array of distance for the vertices
   return parameters:
       none
 */
void print_bfs_matrix_result(int rows, int* color, int* distance)
{
    assert(color);
    assert(distance);

    fprintf(stdout, "---- Print BFS Matrix Result ----\n");
    fprintf(stdout, "Vert\tCol\tDis\n");
    for(int i = 0; i < rows; i++) {
        fprintf(stdout, "%d\t%d\t%d\n", i, color[i], distance[i]);
    }
    fprintf(stdout, "--------\n\n");
}


/* Debugging code that prints a vector
   input parameters:
       int* vector	vector whose content we wish to print
       int  rows	# of elements in the vector
   return parameters:
       none
 */
void print_vector(int* vector, int rows)
{
    assert(vector);

    fprintf(stdout, "---- Print Vector ----\n");
    for(int i = 0; i < rows; i++) {
        fprintf(stdout, "%d\n", vector[i]);
    }
    fprintf(stdout, "--------\n\n");
}


/* Debugging code that prints the content of a 2D matrix
   input parameters:
       int* matrix	2D matrix we wish to print2D matrix we wish to print
       int  rows	# of rows of the input matrix
       int  cols	# of cols of the input matrix
   return parameters:
       none
 */
void print_matrix(int **matrix, int rows, int cols)
{
    assert(matrix);

    fprintf(stdout, "---- Print Matrix ----\n");
    fprintf(stdout, "This matrix is %d x %d\n", rows, cols);
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            fprintf(stdout, "%d ", matrix[i][j]);
        }
        fprintf(stdout, "\n");
    }
    fprintf(stdout, "--------\n\n");

}


/* This function takes in a 2D matrix (src), transposes it and 
   then stores it in a destination 2D matrix (dst).
   Transpose operation takes each src[i][j] element and stores it
   in dst[j][i]
   
   input parameters:
       int** dst	Where you store the transpose of src
                        Dimensions are cols x rows
       int** src	Matrix you want to transpose
                        Dimensions are rows x cols
       int   rows	# of rows of input matrix (src)
       int   cols	# of cols of input matrix (src)
   return parameters:
       none
 */
void matrix_transpose(int** dst, int** src, int rows, int cols)
{
    assert(dst);
    assert(src);
    assert(rows == cols);

    // INSERT YOUR CODE HERE
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++){
            dst[j][i] = src[i][j];
        }
    }
}






/* This function 'resets a vetor to have all
   zero value
   input parameters:
       int* vector	the input vector to reset
       int  rows	the number of elements in the vector
   return parameters:
       none
 */
void reset_vector(int* vector, int rows)
{
    assert(vector);

    for(int i = 0; i < rows; i++) {
        vector[i] = 0;
    }
}


void histo_prefix(int** matrix, int rows, int cols, 
                    int** csr_row_ind, int** csr_col_ind, int** csr_vals, int* pnnz){
    int nnz = 0;
    for(int i = 0; i < rows; i++){
        //count the number of 1s in matrix
        for(int j = 0; j < cols; j++){
            if(matrix[i][j] != 0){
                nnz++;
            }
        }
    }

    int* row_ind = (int*)malloc(sizeof(int)*nnz);
    int* col_ind = (int*)malloc(sizeof(int)*nnz);
    int* vals = (int*)malloc(sizeof(int)*nnz);

    int row_ind_ctr = 0;
    for(int i = 0; i < rows; i++){
        //populate row_ind, col_ind, and vals
        for(int j = 0; j < cols; j++){
            if(matrix[i][j] != 0){
                row_ind[row_ind_ctr] = i;
                col_ind[row_ind_ctr] = j;
                vals[row_ind_ctr] = matrix[i][j];
                row_ind_ctr++;
            }
        }
    }

    //create histogram and prefix sum arrays
    int* histogram = (int*)malloc(sizeof(int)*rows);
    int* prefix_sum = (int*)malloc(rows*sizeof(int));
    memset(histogram, 0, rows*sizeof(int));
    memset(prefix_sum, 0, rows);

    int i = 0;
    int current_row;

    while(i < nnz){
        //printf("%d\n", row_ind[i]);
        current_row = row_ind[i];
        histogram[current_row]++;
        i++;
    }

    //create prefix sum
    //will be an array that holds the indices of the beginnings
    //of each row of non-zero ints
    
    prefix_sum[0] = 0;
    int j = 1;
    while(j < rows){
        //printf("%d", histogram[j]);
        prefix_sum[j] = prefix_sum[j - 1] + histogram[j - 1];
        //printf("%d\n", prefix_sum[j]);
        j++;  
    }

    *csr_row_ind = prefix_sum;
    *csr_col_ind = col_ind;
    *csr_vals = vals;
    *pnnz = nnz;
    free(row_ind);
    free(histogram);
}

/* This function calculates the sparse matrix-vector multiply from the matrix
   in CSR format (i.e., csr_row_ptr, csr_col_ind, and csr_vals) and the vector 
   in an array (i.e., vector_x). It stores the result in another array (i.e.,
   res)
   input parameters:
       these are 'consumed' by this function
       unsigned int** csr_row_ptr	row pointers to csr_col_ind and 
                                        csr_vals in CSR 
       unsigned int** csr_col_ind	column index for the non-zeros in CSR
       double**       csr_vals		values for the non-zeros in CSR
       int            m			# of rows in the matrix
       int            n			# of columns in the matrix
       int            nnz		# of non-zeros in the matrix
       double         vector_x		input vector

       these are 'produced' by this function
       double*        res		Result of SpMV. res = A * x, where
                                        A is stored in CSR format and x is 
                                        stored in vector_x
   return parameters:
       none

 */
void spmv(int* csr_row_ptr, int* csr_col_ind, 
          int* csr_vals, int m, int n, int nnz, 
          int* vector_x, int* res)
{   
    int row_ptr_current = 0;
    int row_ptr_next = 1;
    int ans_array_index = 0;
    int vector_index = 0;
    double multiple = 0;
    /*
    printf("\nprefix sum\n");
    for(int i = 0; i < m; i++){
        printf("vertex %d - %d\n", i, csr_row_ptr[i]);
    }
    */
    for(int i = 0; i < m; i++){
        vector_index = csr_row_ptr[i]; //used to match the val and vector_val to be multiplied together
        multiple = 0; //stores the current multiple
        int end_of_row = csr_row_ptr[row_ptr_next];
        if(row_ptr_next == m) {
            end_of_row = nnz;
        }

        int temp_row_ptr = csr_row_ptr[row_ptr_current]; //used to traverse vals without changing the value of csr_row_ptr
        //while current row_ptr < next_row_ptr then we are in the same row of values
        while(temp_row_ptr < end_of_row) {
            multiple += csr_vals[vector_index] * vector_x[csr_col_ind[vector_index]];
            vector_index++; //move to the next valid value and its corresponding vector value
            temp_row_ptr++; //will move the row_ptr to the next val
        }

        res[i] = multiple; //store multiple in answer array
        row_ptr_current = row_ptr_next; //set current row_ptr to what was next(ie the next row of vals)
        row_ptr_next++; //move next to the next row
    }
}


/* SpMV-based BFS implementation
   input parameters:
   These are 'consumed' by this function
       int** int_array	input array representing the adjacency
                        matrix
       int   rows	# of rows of the adajcency matrix
       int   cols	# of cols of the adajcency matrix
       int   source	source vertex for the BFS
   These are 'produced' by this function
       int*  color	color array
       int*  distance	distance array
   return parameters:
       none
  */
void bfs_spmv(int** int_array, int rows, int cols, int source,
              int* color, int* distance)
{
    // check the input parameters to see if they are valid
    if(rows != cols) {
        fprintf(stderr, "Not an adjacency matrix\n");
	exit(EXIT_FAILURE);
    }
    if(source >= rows) {
        fprintf(stderr, "Invalid source vertex\n");
	exit(EXIT_FAILURE);
    }
    assert(int_array);
    assert(color);
    assert(distance);

    fprintf(stdout, "Breadth first search on the graph using SpMV ... ");

    
    // Transpose the adjacency matrix
    int** mat_trans = NULL;
    init_2d_array(&mat_trans, cols, rows);
    matrix_transpose(mat_trans, int_array, rows, cols);
    print_matrix(int_array, rows, cols);
    print_matrix(mat_trans, rows, cols);
    #if DEBUG
        print_matrix(mat_trans, cols, rows);
    #endif

    // Initialize the various data structures
    int* vec = (int*) malloc(sizeof(int) * rows);
    assert(vec);
    for(int i = 0; i < rows; i++) {
        if(i == source) {
            vec[i] = 1;
            color[i] = 2;
            distance[i] = 0;
        } else {
            vec[i] = 0;
            color[i] = 0;
            distance[i] = -1;
        }
    }
    int* res = (int*) malloc(sizeof(int) * cols);
    assert(res);
    reset_vector(res, cols);


    int iter = 1;
    int done = 0;
    int *src = vec;
    int *dst = res;
    
    
    int* csr_row_ind = NULL;
    int* csr_col_ind = NULL;
    int* csr_vals = NULL;
    int nnz;
    histo_prefix(mat_trans, rows, cols, &csr_row_ind, &csr_col_ind, &csr_vals, &nnz);

    // Do BFS until done
    while(!done) {
        // INSERT YOUR CODE HERE

        // given a vector of source vetices, find the neighbors
        // HINT: spmv
        //perform spmv using vector and column from transposed matrix
        /*
        printf("\nsrc\n");
        for(int i = 0; i < rows; i++){
            printf("vertex %d - %d\n", i, src[i]);
        }
        */

        spmv(csr_row_ind, csr_col_ind, csr_vals, rows, cols, nnz, src, dst);

        /*
        printf("\ndst after spmv\n");
        for(int i = 0; i < rows; i++){
            printf("vertex %d - %d\n", i, dst[i]);
        }
        */

        // color the source vertices for this iteration `black'
        for(int i = 0; i < rows; i++){
            if(src[i] > 0){
                //if > 0 its a source vector, thus paint black
                color[i] = 2;
            }
        }
        for(int i = 0; i < rows; i++){
            if((dst[i] < 0) && color[i] == 0){
                //check that its marked in the dst vector AND not already colored as a source
                //set neighbors to color 1 to mark them as in queue
                color[i] = 1;
            }
        }

        /*
        printf("\ncolors\n");
        for(int i = 0; i < rows; i++){
            printf("vertex %d - %d\n", i, color[i]);
        }
        */

        // store the distance for the newly discovered neighbors
        for(int i = 0; i < rows; i ++){
            if((dst[i] >= 1) && (distance[i] == -1)){
                //vertex's distance is equal to the current iteration
                distance[i] = iter;
            }
        }
        iter++;

        /*
        printf("\ndistance\n");
        for(int i = 0; i < rows; i++){
            printf("vertex %d - %d\n", i, distance[i]);
        }
        */
       
        // Before we begin, eliminate vertices that have already been visited
        for(int i = 0; i < rows; i++){
            if(color[i] == 2){
                dst[i] = 0; //if a vertex was already visited, remove it from the current dst array
            }
        }

        // Check to see if no neighbors were found,
        // in which case, we are done
        int check = 0;
        for(int i = 0; i < rows; i++){
            check += dst[i];
        }
        if(check == 0){
            //if the vector is empty, there are no neighbors left
            //so we are done
            done = 1;
        } else {
            memcpy(src, dst, rows*sizeof(int)); //use dst as the input vector for the next iteration
            reset_vector(dst, rows);
        }
        // iter is equivalent to each `breadth' searched (i.e., distance from
        // the source vertex)
    }

    fprintf(stdout, "done\n");

    #if DEBUG
        print_bfs_matrix_result(rows, color, distance);
    #endif

    free_2d_array(mat_trans, cols);
    free(vec);
    free(res);
    free(csr_row_ind);
    free(csr_col_ind);
    free(csr_vals);
}


/* This function allocates memory for a 2D array of size rows x cols
   input parameters
       int*** arr		reference to the 2D array you want to init
       int    rows		# of rows in the 2D array
       int    cols		# of columns in the 2D array
   return parameters:
        none
 */
void init_2d_array(int*** arr, int rows, int cols)
{
    int** tmpArr = (int**) malloc(sizeof(int*) * rows);
    assert(tmpArr);
    for(int i = 0; i < rows; i++) {
        tmpArr[i] = (int*) malloc(sizeof(int) * cols);
        assert(tmpArr[i]);
    }
    *arr = tmpArr;

}


/* This function frees memory allocated to a 2D array.
   input parameters:
       int** arr	the 2D array to deallocate
       int   rows	# of rows of the matrix
   return parameters:
       none
 */
void free_2d_array(int** arr, int rows)
{
    for(int i = 0; i < rows; i++) {
        // free each row
        free(arr[i]);
    }
    // free the matrix
    free(arr);
}
