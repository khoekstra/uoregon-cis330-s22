#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include "kcipher.h"

struct KCipher::CipherCheshire {
    vector<string> cipher_alpha;
    int page_num;
};


/* Helper function definitions
 */

// -------------------------------------------------------
// Running Key Cipher implementation
// -------------------------------------------------------
KCipher::KCipher()
{
    // TODO: Implement this default constructor
    smile = new CipherCheshire;
    smile->page_num = 0;
    //TODO: figure out what to make the default key
    smile->cipher_alpha.emplace_back("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

}

/* This constructor initiates the object with a
   input cipher key
 */
KCipher::KCipher(string cipher_alpha)
{
    // TODO: Implement this constructor
    smile = new CipherCheshire;
    smile->cipher_alpha.emplace_back(cipher_alpha);
}

/* Destructor
 */
KCipher::~KCipher()
{
    // TODO: Implement this constructor
    //unsure about this
    delete smile;
}

/* This member function encrypts the input text 
   using the intialized cipher key
 */
string KCipher::encrypt(string raw)
{
    cout << "Encrypting...";
    string retStr;
    // TODO: Finish this function
    long unsigned int i = 0;
    
    //remove spaces from cipher text
    string cipher = smile->cipher_alpha[smile->page_num];
    remove(cipher.begin(), cipher.end(), ' ');
    
    while(i < raw.size()){
        string alphabet("abcdefghijklmnopqrstuvwxyz");
        
        //first find where the letter in the message is in the cipher
        if(raw[i] == ' '){
            //if it's a space, do not encrypt
            retStr += raw[i];
        } else {
            //Remember if the raw character is uppercase or not
            bool upper = isupper(raw[i]);
            //numbers for the x axis pos and y axis pos
            unsigned int enc_num_x;
            unsigned int enc_num_y; 
            //if it was upper case
            
            if(upper){
                //first get the x and y pos for the tablu recta
                enc_num_y = find_pos(alphabet, LOWER_CASE(raw[i]));
                
                //always use the next character in cipher
                enc_num_x = find_pos(alphabet, LOWER_CASE(cipher[0]));
                
                //need to rotate alphabet by y positions
                //rotates the opposite direction of rot-13 so 26-enc_num
                rotate_string(alphabet, (26-enc_num_y));
                //encode character to return string
                retStr += UPPER_CASE(alphabet[enc_num_x]);
                //now remove front char from cipher to make next available
                cipher.erase(0, 1);

            } else {
                //first get the x and y pos for the tablu recta
                enc_num_y = find_pos(alphabet, LOWER_CASE(raw[i]));
                
                //always use the next character in cipher
                enc_num_x = find_pos(alphabet, LOWER_CASE(cipher[0]));
                
                //need to rotate alphabet by y positions
                //rotates the opposite direction of rot-13 so 26-enc_num
                rotate_string(alphabet, (26-enc_num_y));
                //encode character to return string
                retStr += alphabet[enc_num_x];
                //now remove front char from cipher to make next available
                cipher.erase(0, 1);
            }
        }
        i++;
    }

    cout << "Done" << endl;

    return retStr;
}


/* This member function decrypts the input text 
   using the intialized cipher key
 */
string KCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting...";
    // TODO: Finish this function
    long unsigned int i = 0;
    
    //remove spaces from cipher text
    string cipher = smile->cipher_alpha[smile->page_num];
    remove(cipher.begin(), cipher.end(), ' ');
    

    while(i < enc.size()){
        //need a rotatable alphabet and unmovable one
        string alphabet("abcdefghijklmnopqrstuvwxyz");
        string const ALPHABET("abcdefghijklmnopqrstuvwxyz");
        //first find where the letter in the message is in the cipher
        if(enc[i] == ' '){
            //if it's a space, do not encrypt
            retStr += enc[i];
        } else {
            //Remember if the raw character is uppercase or not
            bool upper = isupper(enc[i]);
            //numbers for the x axis pos and y axis pos
            unsigned int enc_num_x;
            unsigned int enc_num_y; 
            //if it was upper case
            
            if(upper){
                //first get the x and y pos for the tablu recta
                //always use the next character in cipher
                enc_num_x = find_pos(alphabet, LOWER_CASE(cipher[0]));
                
                //need to rotate alphabet by y positions
                //rotates the opposite direction of rot-13 so 26-enc_num
                rotate_string(alphabet, (26-enc_num_x));
                //find the encrypted letter in the rotated alphabet
                enc_num_y = find_pos(alphabet, LOWER_CASE(enc[i]));
                
                //encode character to return string
                retStr += UPPER_CASE(ALPHABET[enc_num_y]);
                //now remove front char from cipher to make next available
                cipher.erase(0, 1);

            } else {
                //first get the x and y pos for the tablu recta
                //always use the next character in cipher
                enc_num_x = find_pos(alphabet, LOWER_CASE(cipher[0]));
                
                //need to rotate alphabet by y positions
                //rotates the opposite direction of rot-13 so 26-enc_num
                rotate_string(alphabet, (26-enc_num_x));
                //find the encrypted letter in the rotated alphabet
                enc_num_y = find_pos(alphabet, LOWER_CASE(enc[i]));
                
                //encode character to return string
                retStr += ALPHABET[enc_num_y];
                //now remove front char from cipher to make next available
                cipher.erase(0, 1);
            }
        }
        i++;
    }
    cout << "Done" << endl;

    return retStr;

}

void KCipher::add_key(string page){
    smile->cipher_alpha.emplace_back(page);
    
}

void KCipher::set_id(int page_num = 0){
    smile->page_num = page_num;
}

