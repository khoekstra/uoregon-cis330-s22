#include <string>
#include <iostream>
#include <algorithm>
#include "ccipher.h"


// -------------------------------------------------------
// Caesar Cipher implementation
/* Cheshire smile implementation.
   It only contains the cipher alphabet
 */
struct CCipher::CipherCheshire {
    int key;
    string cipher_alpha;
};

// -------------------------------------------------------
// -------------------------------------------------------

/* Default constructor
   This will actually not encrypt the input text
   because it's using the unscrambled alphabet
 */
CCipher::CCipher()
{
    // TODO: Implement this default constructor
    smile = new CipherCheshire;
    smile->key = 0;
    smile->cipher_alpha = "abcdefghijklmnopqrstuvwxyz";

}

/* This constructor initiates the object with a
   input cipher key
 */
CCipher::CCipher(int key)
{
    // TODO: Implement this constructor
    smile = new CipherCheshire;
    smile->key = key;
    std::string alphabet("abcdefghijklmnopqrstuvwxyz");
    //rotate alphabet by key
    rotate_string(alphabet, key);
    smile->cipher_alpha = alphabet;
}

/* Destructor
 */
CCipher::~CCipher()
{
    // TODO: Implement this constructor
    //unsure about this
    delete smile;
}

/* This member function encrypts the input text 
   using the intialized cipher key
 */
string CCipher::encrypt(string raw)
{
    cout << "Encrypting...";
    string retStr;
    // TODO: Finish this function
    long unsigned int i = 0;
    std::string alphabet("abcdefghijklmnopqrstuvwxyz");
    while(i < raw.size()){
        //first find where the letter in the message is in the cipher
        if(raw[i] == ' '){
            //if it's a space, do not encrypt
            retStr += raw[i];
        } else {
            //Remember if the raw character is uppercase or not
            bool upper = isupper(raw[i]);
            unsigned int enc_num; 
            //if it was upper case
            if(upper){
                //first find where the letter in the message is in the cipher
                enc_num = find_pos(smile->cipher_alpha, LOWER_CASE(raw[i]));
                //then set the letter in the return string to the letter in the alphabet
                //in the spot we determined in the cipher
                retStr += UPPER_CASE(alphabet[enc_num]);
            } else {
                //first find where the letter in the message is in the cipher
                enc_num = find_pos(smile->cipher_alpha, raw[i]);
                //then set the letter in the return string to the letter in the alphabet
                //in the spot we determined in the cipher
                retStr += alphabet[enc_num];
            }
        }
        i++;
    }
    cout << "Done" << endl;

    return retStr;
}


/* This member function decrypts the input text 
   using the intialized cipher key
 */
string CCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting...";
    // TODO: Finish this function
    long unsigned int i = 0;
    std::string alphabet("abcdefghijklmnopqrstuvwxyz");
    while(i < enc.size()){
        //first find where the letter in the message is in the cipher
        if(enc[i] == ' '){
            //if it's a space, do not encrypt
            retStr += enc[i];
        } else {
            //Remember if the raw character is uppercase or not
            bool upper = isupper(enc[i]);
            unsigned int enc_num; 
            //if it was upper case
            if(upper){
                //first find where the letter in the message is in the cipher
                enc_num = find_pos(alphabet, LOWER_CASE(enc[i]));
                //then set the letter in the return string to the letter in the alphabet
                //in the spot we determined in the cipher
                retStr += UPPER_CASE(smile->cipher_alpha[enc_num]);
            } else {
                //first find where the letter in the message is in the cipher
                enc_num = find_pos(alphabet, enc[i]);
                //then set the letter in the return string to the letter in the alphabet
                //in the spot we determined in the cipher
                retStr += smile->cipher_alpha[enc_num];
            }
        }
        i++;
    }

    cout << "Done" << endl;

    return retStr;

}


// Rotates the input string in_str by rot positions
void rotate_string(string& in_str, int rot)
{
    // TODO: You will likely need this function. Implement it.
    //rotations >= 26 are redundant
    while(rot >= 26){
        rot -= 26;
    }
    string temp_rot = in_str.substr(0, (26 - rot)); //retrieve the rot number of characters from the front of the input string
    in_str.erase(0, (26 - rot)); //now remove those characters from the input string
    in_str.append(temp_rot); //finally put the retreived characters and place them on the end of input string
                            //to complete the rotation
}
