<?php
include 'connectionData.php';

// Get the points from the form
$points = $_POST['Points'];

// Sanitize input
$points = intval($points);

// SQL query to find players
$sql = "SELECT player_name, points
        FROM players
        WHERE points > ? 
        AND game_date BETWEEN '2022-10-19' AND '2022-10-31'";

// Prepare and bind
$stmt = $conn->prepare($sql);
$stmt->bind_param("i", $points);
$stmt->execute();
$result = $stmt->get_result();

echo "<h3>Players who scored more than $points points:</h3>";
echo "<table border='1'>";
echo "<tr><th>Player Name</th><th>Points</th></tr>";

// Fetch and display the results
while($row = $result->fetch_assoc()) {
    echo "<tr><td>" . $row['player_name'] . "</td><td>" . $row['points'] . "</td></tr>";
}

echo "</table>";

$stmt->close();
$conn->close();
?>